document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
	var ui = document.getElementById("camera-ui");
	if (navigator.camera) {
		ui.innerHTML = "<a href=\"javascript:takePhoto(true)\">Add Photo</a>"
	} else {
		ui.innerHTML = "Camera not supported on your device";
	}
}
function takePhoto(firstrun) {
	var options = {
		/*quality: 50,*/
		destinationType: Camera.DestinationType.DATA_URL, /* DATA_URL, FILE_URI, NATIVE_URI */
		sourceType: Camera.PictureSourceType.CAMERA, /* PHOTOLIBRARY, CAMERA, SAVEDPHOTOALBUM */
		allowEdit: true,
		encodingType: Camera.EncodingType.JPEG, /* JPEG, PNG */
		/*targetWidth: 300,*/
		/*targetHeight: 300,*/
		mediaType: Camera.MediaType.PICTURE, /* PICTURE, VIDEO, ALLMEDIA */
		correctOrientation: true,
		/* saveToPhotoAlbum: false, */
		/* popoverOptions: (iOS only), */
		cameraDirection: Camera.Direction.BACK /* BACK, FRONT */
	};
	navigator.camera.getPicture(
		function(imgData) {
			if (firstrun) {
				var ui = document.getElementById("camera-ui");
				ui.innerHTML = "<a href=\"javascript:takePhoto(false)\">Change Photo</a><img id=thumbnail />"
			}
			var input = document.getElementById("photo");
			var img = document.getElementById("thumbnail");
			img.src = "data:image/jpeg;base64,"+imgData;
			input.value = imgData;
		},
		function() {
			alert("Error taking picture", "Error");
		},
		options);
}
