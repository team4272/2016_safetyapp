SCSS = scss
CORDOVA = cordova
GSED = sed
WGET = wget -c --no-use-server-timestamp

pdfjs = https://github.com/mozilla/pdf.js/releases/download/v1.4.20/pdfjs-1.4.20-dist.zip
frc_safety_manual = http://www.firstinspires.org/sites/default/files/uploads/resource_library/frc/team-resources/safety/2016/frc-team-safety-manual.pdf

platforms = android browser

sources = \
	www/FIRST_Safety_Manual.pdf \
	www/pdfjs \
	www/css/index.css \
	www/MSDS.html

browser-targets = platforms/browser/build/package.zip

android-targets = platforms/android/build/outputs/apk/android-debug.apk

android-resources = \
	platforms/android/res/drawable/icon.png \
	platforms/android/res/drawable-ldpi/icon.png \
	platforms/android/res/drawable-mdpi/icon.png \
	platforms/android/res/drawable-hdpi/icon.png \
	platforms/android/res/drawable-xhdpi/icon.png \
	\
	platforms/android/res/drawable-land-ldpi/screen.png \
	platforms/android/res/drawable-land-mdpi/screen.png \
	platforms/android/res/drawable-land-hdpi/screen.png \
	platforms/android/res/drawable-land-xhdpi/screen.png \
	\
	platforms/android/res/drawable-port-ldpi/screen.png \
	platforms/android/res/drawable-port-mdpi/screen.png \
	platforms/android/res/drawable-port-hdpi/screen.png \
	platforms/android/res/drawable-port-xhdpi/screen.png

targets = $(foreach platform,$(platforms),$($(platform)-targets))
resources = $(foreach platform,$(platforms),$($(platform)-resources))

all: build

sources: PHONY $(sources)
resources: PHONY $(resources)
build: $(targets)

clean-sources: PHONY
	rm -rf -- www/pdfjs
	rm -f -- $(sources)
clean-resources: PHONY
	rm -f -- $(resources)
clean: PHONY clean-sources clean-resources
	rm -f -- info.txt www/css/*.map pdfjs-*.zip hs_err_pid*.log
distclean: PHONY clean
	rm -rf -- platforms plugins

$(foreach platform,$(platforms),\
          $(eval clean-$(platform)-resources: PHONY; rm -f -- $$($(platform)-resources)) \
          $(eval $(platform)-resources: PHONY $$($(platform)-resources)) \
          $(eval build-$(platform): PHONY $$($(platform)-targets)))

upload: PHONY build
	git push
	rsync -rv --delete -e ssh platforms/browser/www/ lukeshu@lukeshu.com:/srv/frc/app/web
	rsync -v -e ssh platforms/android/build/outputs/apk/android-debug.apk lukeshu@lukeshu.com:/srv/frc/releases/SafetyApp-latest.apk


DEP_DIR = $1 $(shell find $1)

platforms/browser/build/package.zip: platforms/browser config.xml $(sources) $(call DEP_DIR,www)
	rm -f -- $@
	$(CORDOVA) build browser
	test -f $@

platforms/android/build/outputs/apk/android-debug.apk: platforms/android config.xml $(sources) $(call DEP_DIR,www)
	rm -f -- $@
	$(CORDOVA) build android
	test -f $@

platforms/%:
	$(CORDOVA) platform add $*

www/FIRST_Safety_Manual.pdf:
	$(WGET) -O $@ $(frc_safety_manual)

$(notdir $(pdfjs)):
	$(WGET) $(pdfjs)

www/pdfjs: $(notdir $(pdfjs))
	rm -rf -- $@
	mkdir -- $@ && bsdtar -xf $(abspath $<) -C $@ --exclude '*.pdf' || rm -rf -- $@

www/MSDS.html: www/MSDS.html.in www/MSDS Makefile
	ls www/MSDS | $(GSED) 'p;s/_/ /g' | $(GSED) -r 'N;s,^(.*)\n(.*)\.pdf,<a href="pdfjs/web/viewer.html?file=../../MSDS/\1">\2</a>,' | $(GSED) $$'/@list@/{ s/@list@//; r/dev/stdin\n}' www/MSDS.html.in > $@


platforms/android/res/drawable/icon.png: www/img/logo.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 96x96 $@
platforms/android/res/drawable-ldpi/icon.png: www/img/logo.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 36x36 $@
platforms/android/res/drawable-mdpi/icon.png: www/img/logo.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 48x48 $@
platforms/android/res/drawable-hdpi/icon.png: www/img/logo.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 72x72 $@
platforms/android/res/drawable-xhdpi/icon.png: www/img/logo.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 96x96 $@

platforms/android/res/drawable-land-ldpi/screen.png: www/img/screen-landscape.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 320x200 $@
platforms/android/res/drawable-land-mdpi/screen.png: www/img/screen-landscape.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 480x320 $@
platforms/android/res/drawable-land-hdpi/screen.png: www/img/screen-landscape.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 800x480 $@
platforms/android/res/drawable-land-xhdpi/screen.png: www/img/screen-landscape.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 1280x720 $@

platforms/android/res/drawable-port-ldpi/screen.png: www/img/screen-portrait.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 200x320 $@
platforms/android/res/drawable-port-mdpi/screen.png: www/img/screen-portrait.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 320x480 $@
platforms/android/res/drawable-port-hdpi/screen.png: www/img/screen-portrait.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 480x800 $@
platforms/android/res/drawable-port-xhdpi/screen.png: www/img/screen-portrait.png | platforms/android
	mkdir -p $(@D)
	convert $< -resize 720x1280 $@


%.css: %.scss
	$(SCSS) $< $@


.PHONY: PHONY FORCE
.DELETE_ON_ERROR:
.SECONDARY:
